package com.gitee.dbswitch.product.postgresql.copy.configuration;

public interface IConfiguration {

  int getBufferSize();
}
